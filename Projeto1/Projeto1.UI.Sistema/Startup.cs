﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Projeto1.UI.Sistema.Startup))]
namespace Projeto1.UI.Sistema
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
